package cn.tedu.sp06.fb;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
public class ItemFB implements FallbackProvider {
    /*
    设置当前降级类，是针对哪个服务进行降级
       - item-service： 只针对商品降级
       - *：    对所有服务都应用当前降级类
       - null： 对所有服务都应用当前降级类
     */
    @Override
    public String getRoute() {
        return "item-service";
    }

    /*
    向客户端返回的响应
     */
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new ClientHttpResponse() {
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }
            public int getRawStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.value();
            }
            public String getStatusText() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }
            public void close() {
                // 关闭下面方法中的流
                // ByteArrayInputStream 不占用任何底层系统资源，
                // 所以不需要关闭
            }
            public InputStream getBody() throws IOException {
                // JsonResult - {code:500,msg:调用后台服务出错,data:null}
                String json =
                        JsonResult.build().code(500).msg("调用后台服务出错").toString();
                // 把 json 封装到 ByteArrayInputStream
                return new ByteArrayInputStream(json.getBytes("UTF-8"));
            }
            public HttpHeaders getHeaders() {
                HttpHeaders h = new HttpHeaders();
                h.add("Content-Type", "application/json;charset=UTF-8");
                return h;
            }
        };
    }
}
